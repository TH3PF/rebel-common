import std.stdio;

import rebel.base;
import rebel.scenario.event;
import rebel.scenario.state;
import rebel.utils.geometry.vector3D;
import rebel.utils.geometry.integration;
import rebel.scenario.world;
import rebel.scenario.entity;
import rebel.scenario.entityComponents.compoSpacial;
import rebel.scenario.entityComponents.compoController;

import std.conv;

class TestState: State{
private:
	World world;
public:
	this(){
		world = new World(vec3i(64, 64, 64), vec3i(16, 16, 16));
		assert(world.chunks.length == 64);
	}
	void Advance(ref Base base) {}
	void Recede(ref Base base) {}
	void Listen(ref Base base) {

	}
	void Render(ref Base base) {}
}

/** Test application */
int main()
{
	Event ev = new Event("", "", (ref Event event){ 
		
	});

	Base base = new Base([
		"Test" : new TestState()
	],"Test", 1.0);
	EulerIntegrator integrator = new EulerIntegrator();
	Entity e = new Entity([
		"Spacial" : new ComponentSpacial(5, true, integrator),
	]);

	ComponentSpacial cs = cast(ComponentSpacial) e.Spacial;
	return 0;
}
