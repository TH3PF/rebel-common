module rebel.scenario.world;

import rebel.base;
import rebel.interactive;
import rebel.scenario.entity;
import rebel.utils.geometry.vector3D;

class Chunk : Interactive{
	void Advance(ref Base base){
		
	}
	
	void Recede(ref Base base){
		
	}
	
	void Listen(ref Base base){
		
	}
	
	void Render(ref Base base){
		
	}
}

class World : Interactive{
protected:
	immutable vec3i size, chunkSize;
	public Chunk[] chunks;
	Entity[] entities;

public:
	this(vec3i size, vec3i chunkSize){
		this.size = size;
		this.chunkSize = chunkSize;
		chunks = new Chunk[(size.x / chunkSize.x) * 
						   (size.y / chunkSize.y) * 
						   (size.z / chunkSize.z)
		];
		for(int i = 0; i < chunks.length; i++)
			chunks[i] = new Chunk();
	}

	void SpawnEntity(Entity entity){
		entities ~= entity;
	}


	void Advance(ref Base base){

	}

	void Recede(ref Base base){

	}

	void Listen(ref Base base){

	}

	void Render(ref Base base){

	}
}
