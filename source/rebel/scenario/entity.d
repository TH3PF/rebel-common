module rebel.scenario.entity;

import rebel.base;
import rebel.interactive;

import std.stdio;
import core.vararg;

interface EntityComponent : Interactive{
	/** Handles other function calls */
	void opDispatch(string m)();
}

class Entity : Interactive {
private:

protected:
	EntityComponent[string] components;

public:
	this(EntityComponent[string] components){
		this.components = components;
	}

	/** Handles component specific function calls */
	EntityComponent opDispatch(string component)(){
		return components[component];
	}
public override:
	/** Advances each component one time step further */
	void Advance(ref Base base){
		foreach(EntityComponent c; components)
			c.Advance(base);
	}
	/** Recedes each component one time step backwards */
	void Recede(ref Base base){
		foreach(EntityComponent c; components)
			c.Recede(base);
	}
	/** Let's each component push their events */
	void Listen(ref Base base){
		foreach(EntityComponent c; components)
			c.Listen(base);
	}
	/** Renders each component visible or audible, if neccessary */
	void Render(ref Base base){
		foreach(EntityComponent c; components)
			c.Render(base);
	}
}