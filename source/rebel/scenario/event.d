module rebel.scenario.event;

class Event{
private:
	string name, desc;
	void delegate(ref Event event) execution;

public:
	this(string name, string description, void delegate(ref Event event) execution){
		this.name = name;
		this.desc = description;
	}

	void opCall(){
		execution(this);
	}
}