module rebel.scenario.entityComponents.compoSpacial;

import rebel.base;
import rebel.scenario.entity;
import rebel.utils.geometry.vector3D;
import rebel.utils.geometry.integration;

/** Will be moved the Rebel-Physics */
class ComponentSpacial : EntityComponent{
private:
	Integrator integrator;
public:
	vec3d Location, Velocity, Acceleration;
	double Mass;
	bool Mobile;

	this(double mass, bool mobile, Integrator integrator){
		this.Mass = Mass;
		this.Mobile = mobile;
		this.integrator = integrator;
	}

	void opDispatch(string m)(){
		CastForce(args);
		break;
	}

	void CastForce(vec3d force, vec3d displacement = vec3(.0, .0, .0)){
		if (Mobile) {
			force *= vec3d(1.0,1.0,1.0) * vec3d(1.0/Mass, 1.0/Mass, 1.0/Mass);
			Acceleration += force;
			//rotationalForces += force.cross(
			//		(Location + displacement) - Location);
		}
	}

public override:
	void Advance(ref Base base){
		integrator(Location.x, Velocity.x, Velocity.x, Acceleration.x, base.Delta);
	}
	void Recede(ref Base base){
		integrator(Location.x, Velocity.x, -Velocity.x, -Acceleration.x, base.Delta);
	}
	void Listen(ref Base base){

	}
	void Render(ref Base base){

	}
}
