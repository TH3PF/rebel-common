module rebel.base;

import rebel.interactive;
import rebel.scenario.state;
import rebel.scenario.event;

import std.array;

class Base{
private:
	double timeStep, timeScale;
	State[string] states;
	string activeState;
	Event[] events;

public:
	this(State[string] states, string activeState, double timeScale){
		// Checks whether the active state actually is present in the list of states provided
		assert(states.get(activeState, null));
		this.activeState = activeState;

		timeStep = 1.0; //Avoid divides by zero
		this.timeScale = timeScale;
	}

	/** */
	@property ref State ActiveState(){
		return states[activeState];
	}

	/** The time difference in miliseconds since the last update */
	@property double Delta(){
		return timeStep * timeScale;
	}
	/** An estimation of how many ticks are produced per second */
	@property double FPS(){
		return (timeStep != .0) ? (1000.0 / timeStep) : .0;
	}
	/*
	// Queue interface
	@disable Event PollEvent(){

	}
	*/
}