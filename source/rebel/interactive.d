module rebel.interactive;

import rebel.base;

interface Interactive{
	/** Advances the object one time step further */
	void Advance(ref Base base);
	/** Recedes the object one time step backwards */
	void Recede(ref Base base);
	
	/** Let's the object tell what's on it's mind, if neccessary */
	void Listen(ref Base base);
	/** Renders the object visible or audible, if neccessary */
	void Render(ref Base base);
}