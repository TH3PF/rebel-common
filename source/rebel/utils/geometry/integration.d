module rebel.utils.geometry.integration;

/** Interface for solving integrational problems
 *  See_Also: http://en.wikipedia.org/wiki/Integration
*/
interface Integrator{
	// Integrates the problem passed through arguments
	void opCall(ref double x, ref double y, double initial, double derative, double delta);
}

/** Basic implementation of the Euler integration method
 *  See_Also: http://en.wikipedia.org/wiki/Euler_method
*/
class EulerIntegrator : Integrator{
	void opCall(ref double x, ref double y, double initial, double derative, double delta){
		y += derative * delta;
		x += y * delta;
	}
}