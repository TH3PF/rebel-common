module rebel.utils.geometry.vector3D;

public import gl3n.linalg;

void abs(vec3d vector){
	vector.x = (0 < vector.x) ? vector.x : -vector.x;
	vector.y = (0 < vector.y) ? vector.y : -vector.y;
	vector.z = (0 < vector.z) ? vector.z : -vector.z;
}

void clamp(vec3d vector, double min, double max){
	if(vector.x < min)
		vector.x = min;
	else if(max < vector.x)
		vector.x = max;

	if(vector.y < min)
		vector.y = min;
	else if(max < vector.y)
		vector.y = max;

	if(vector.z < min)
		vector.z = min;
	else if(max < vector.z)
		vector.z = max;
}
